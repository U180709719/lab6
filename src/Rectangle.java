public class Rectangle {

    private int sideA;
    private int sideB;
    private Point topLeft;

    public Rectangle(int sideA, int sideB, Point topLeft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = topLeft;
    }

    public int area() {
        return sideA * sideB;
    }

    public int perimeter() {
        return 2 * sideA + 2 * sideB;
    }

    public Point[] corners() {

        Point topRight = new Point(topLeft.xCoord+ sideA, topLeft.yCoord);
        Point bottomLeft = new Point(topLeft.xCoord, topLeft.yCoord - sideB);
        Point bottomRight = new Point(topLeft.xCoord + sideA, topLeft.yCoord - sideB);

        Point corners[] = new Point[4];
        corners[0] = topLeft;
        corners[1] = topRight;
        corners[2] = bottomLeft;
        corners[3] = bottomRight;
        return corners;

    }

}