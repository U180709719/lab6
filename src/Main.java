public class Main {

    public static void main(String[] args) {
        int sideA = 11;
        int sideB = 12;
        Point topLeft = new Point(2, 7);
        Rectangle rectangle = new Rectangle(sideA, sideB, topLeft);

        System.out.println("\nThe area and perimeter of the instance Rectangle:");
        System.out.println("Area of Rectangle = " + rectangle.area());
        System.out.println("Perimeter of Rectangle = " + rectangle.perimeter());

        Point[] points = rectangle.corners();
        System.out.println("\nThe corners of the rectangle :");
        System.out.println("Top Left Point Coordinates: "+points[0]);
        System.out.println("Top Right Point Coordinates: "+points[1]);
        System.out.println("Bottom Left Point Coordinates: "+points[2]);
        System.out.println("Bottom Right Point Coordinates: "+points[3]);

        Circle circle1 = new Circle(10);
        System.out.println("\nThe area and perimeter of the instance Circle :");
        System.out.println("Area of Circle = "+ circle1.area());
        System.out.println("Perimeter of Circle = " + circle1.perimeter());
        Circle circle2 = new Circle(new Point(2, 3), 5);
        System.out.println("\nCheck whether these two circles intersect or not:");

        if (circle1.isIntersect(circle2)) {
            System.out.println("Circle intersects");

        } else System.out.println("Circle do not intersect");
    }

}