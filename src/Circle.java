class Circle    {

    private Point center;
    private double radius;

    public Circle(double r) {
        center = new Point(0,0);
        radius = r;
    }

    public Circle(Point c, double r) {
        center = c;
        radius = r;
    }
    public double area() {
        return Math.PI*radius*radius;
    }
    public double perimeter() {
        return 2*Math.PI*radius;
    }
    public boolean isIntersect(Circle cercle2) {
        double x = (center.xCoord - cercle2.center.xCoord);
        double y = (center.yCoord - cercle2.center.yCoord);
        double dis = Math.sqrt((x*x) + (y*y));

        return dis <= (radius + cercle2.radius);

    }
}